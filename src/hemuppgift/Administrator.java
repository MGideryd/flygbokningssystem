package hemuppgift;

import java.sql.ResultSet;
import java.util.Scanner;

public class Administrator {

	static Scanner input = new Scanner(System.in);

	DbKoppling koppling = new DbKoppling();
	Validering validera = new Validering();
	Flyglinje flyglinje = new Flyglinje();

	Administrator() {

	}

	public void menyAdmin() {

		int menyVal = 0;

		do {
			System.out.println("1. Skapa flygplats.");
			System.out.println("2. Skapa flybolag.");
			System.out.println("3. Skapa flyglinje.");
			System.out.println("4. Lista flyglinjer.");
			System.out.println("5. Lista alla bokningar.");
			System.out.println("6. Startmeny.");
			menyVal = input.nextInt();
			switch (menyVal) {
			case 1:
				skapaFlygplats();
				break;
			case 2:
				skapaFlygbolag();
				break;
			case 3:
				skapaFlyglinje();
				break;
			case 4:
				listaFlyglinjer();
				break;
			case 5:
				listaBokningar();
				break;
			}
		} while (!(menyVal >= 6));

	}

	public void skapaFlygplats() {

		String val = null;

		do {
			System.out.println("Ange flygplatsnamn.");
			val = input.next();
			if (!validera.valideringFlygplats(val)) {
				System.out.println("Namnet m�ste innh�lla 3 bokst�ver, f�rs�k igen!");
			}
		} while (!validera.valideringFlygplats(val));
		Flygplats flygplats = new Flygplats(val);
		koppling.inmatningDb("INSERT IGNORE INTO " + DbKoppling.databas
				+ ".flygplats(flygplats_namn) VALUES ('" + flygplats.getNamn()
				+ "')");
	}

	public void skapaFlygbolag() {

		String val = null;

		do {
			System.out.println("Ange flygbolagsnamn.");
			val = input.next();
			if (!validera.valideringFlygbolag(val)) {
				System.out.println("Namnet m�ste inneh�lla minst 6 bokst�ver, f�rs�k igen!");
			}
		} while (!validera.valideringFlygbolag(val));
		Flygbolag flygbolag = new Flygbolag(val);
		koppling.inmatningDb("INSERT IGNORE INTO " + DbKoppling.databas
				+ ".flygbolag(flygbolag_namn) VALUES ('" + flygbolag.getNamn()
				+ "')");
	}

	public void skapaFlyglinje() {

		String query;
		int kolid = 0;
		String datum = null;

		koppling.listaFlygplatser();
		
		System.out.println("V�lj avg�ngsflygplats via id.");
		kolid = input.nextInt();

		query = "SELECT flygplats_namn from bokningssystem.flygplats where flygplats_id ="
				+ kolid + "";

		String[] avgangsFlygplats = koppling.hamtaData(query);
		flyglinje.setAvgang(avgangsFlygplats[0]);
		System.out.println(avgangsFlygplats[0]);
		
	
		System.out.println("V�lj ankomstflyplats via id.");
		kolid = input.nextInt();

		query = "SELECT flygplats_namn from bokningssystem.flygplats where flygplats_id ="
				+ kolid + "";

		String[] ankomstFlygplats = koppling.hamtaData(query);
		flyglinje.setAnkomst(ankomstFlygplats[0]);
		System.out.println(ankomstFlygplats[0]);
		
		koppling.listaFlygbolag();
		
		System.out.println("V�lj flygbolag via id.");
		kolid = input.nextInt();

		query = "SELECT flygbolag_namn from bokningssystem.flygbolag where flygbolag_id ="
				+ kolid + "";

		String[] flygbolag = koppling.hamtaData(query);
		flyglinje.setFlygbolag(flygbolag[0]);
		System.out.println(flygbolag[0]);
		
		System.out.println("Ange datum.");
		datum = input.next();
		
		koppling.inmatningDb("INSERT IGNORE INTO "+DbKoppling.databas +".flyglinje(avgang, destination, flygbolag, datum) VALUES"
				+ " ('"+flyglinje.getAvgang()+"',"
				+ " '" +flyglinje.getAnkomst()+ "',"
				+ " '" +flyglinje.getFlygbolag()+ "',"
				+ " '" +datum+ "')");
		
		System.out.println("Flyglinje �r nu skapad.");
		System.out.println("------------------");
		System.out.println(avgangsFlygplats[0]+ "\t"+ankomstFlygplats[0]+"\t"+flygbolag[0]+"\t"+datum);
		System.out.println("------------------");
	}

	public void listaFlyglinjer() {

		koppling.listaFlyglinjer();
	}

	public void listaBokningar() {

		koppling.listaBokningar();
	}
}

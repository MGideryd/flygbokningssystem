package hemuppgift;

import java.util.Scanner;

public class Resenar {

	Scanner input = new Scanner(System.in);
	int menyVal;
	String val;
	Flyglinje flyglinje = new Flyglinje();
	DbKoppling koppling = new DbKoppling();
	
	public void menyResenar() {

		do {
			System.out.println("1. Se tillg�ngliga flyg.");
			System.out.println("2. Boka biljett.");
			System.out.println("3. Se mina bokningar.");
			System.out.println("4. Startmeny.");
			menyVal = input.nextInt();
			switch (menyVal) {
			case 1:
				seFlyglinjer();
				break;
			case 2:
				bokaBiljett();
				break;
			case 3:
				seBokning();
				break;
			}

		} while (!(menyVal >= 4));
	}

	public void seFlyglinjer() {

		koppling.listaFlyglinjer();
	
	}

	public void bokaBiljett() {
		String query;
		String kolid;
		int val = 0;
		String fornamn;
		String efternamn;
		String telenummer;
		int boka;

		koppling.listaFlygplatser();

		System.out.println("Vart vill du flyga fr�n? (ange prefix)");
		kolid = input.next();

		query = "SELECT * from bokningssystem.flyglinje where avgang ='"
				+ kolid + "'";

		String[] bokaFlyg = koppling.listFlights(query);
		System.out.println("------------------");
		for (int i = 0; i < bokaFlyg.length; i++) {
			System.out.print(bokaFlyg[i] + "\n");
		}
		System.out.println("------------------");
		System.out.println("\n" + "V�lj den avg�ng du vill boka via id.");
		boka = input.nextInt();
		
		query = "SELECT * from bokningssystem.flyglinje where flyglinje_id ='"
				+ boka + "'";

		String[] flyglinje = koppling.listFlights(query);
		System.out.println("------------------");
		for (int i = 0; i < flyglinje.length; i++) {
			System.out.print(flyglinje[i] + "\n");
		}
		System.out.println("------------------");
		System.out.println("\n" + "Vill du boka denna avg�ng? 1. Ja 2. Avsluta");
		val = input.nextInt();

		switch (val) {
		case 1:

			System.out.println("Ange ditt f�rnamn.");
			fornamn = input.next();

			System.out.println("Efternamn.");
			efternamn = input.next();

			System.out.println("Telefonnummer.");
			telenummer = input.next();

			int kundid = koppling.inmatningDbPk("INSERT INTO "
					+ DbKoppling.databas
					+ ".kund(fornamn,efternamn,telefonnummer) VALUES ('"
					+ fornamn + "','" + efternamn + "','" + telenummer + "') ",
					new String[] { "kund_id" });

			koppling.inmatningDb("INSERT INTO " + DbKoppling.databas
					+ ".bokning(kund_id, flyglinje) VALUES ('" + kundid + "','"
					+ boka + "')");

			System.out.println("\n" + "Din resa �r nu bokad.");
			
			System.out.println("------------------");
			System.out.println("Flyglinje.");
			
			for (int i = 0; i < flyglinje.length; i++) {
				System.out.print(flyglinje[i] + "\t");
			}
			System.out.println("------------------");
			System.out.println("\n" + "Resen�r.");

			System.out.println(fornamn + "\t" + efternamn + "\t" + telenummer);
			System.out.println("------------------");
		case 2:
			break;
		}

	}

	public void seBokning() {
		String query;
		String sebokning;
		System.out.println("Ange ditt f�rnamn.");
		sebokning = input.next();

		query = "SELECT bokning_id, fornamn, efternamn, telefonnummer, avgang,destination,flygbolag,datum "
				+ "FROM bokningssystem.kund, bokningssystem.bokning, bokningssystem.flyglinje where fornamn like '"+sebokning+"' AND kund.kund_id = bokning.kund_id AND bokning.flyglinje = flyglinje.flyglinje_id";

		System.out.println("\n------------------");
		
		String[] seFlyg = koppling.listFlights(query);
		for (int i = 0; i < seFlyg.length; i++) {
			System.out.println(seFlyg[i]);
		}
		System.out.println("------------------\n");
	}

}

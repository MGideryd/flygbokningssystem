package hemuppgift;

import java.util.Scanner;
public class Main {

	
	public static void main(String[] args) {
				
		DbKoppling dbkopp = new DbKoppling();
		//Anropar metod som skapar databasen.
		dbkopp.skapaDb();
				
		//Startar inloggningsmenyn.
		inloggning();		
		}
		
	public static void inloggning(){
		Scanner input = new Scanner(System.in);
		int inlogg;
		
		Administrator admin = new Administrator();
		Resenar kund = new Resenar();
		
			do{
				System.out.println("Logga in:\n");
				System.out.println("-----------");
				System.out.println("1. Resen�r.");
				System.out.println("2. Admin.");
				System.out.println("3. Exit.");
				System.out.println("-----------");
				inlogg = input.nextInt();
				
				switch(inlogg){
					case 1:
						kund.menyResenar();
						break;
					case 2:
						admin.menyAdmin();
						break;
				}
				if(inlogg > 3){
					System.out.println("F�rs�k igen!");
				}
			}while(!(inlogg == 3));
	
		}

}

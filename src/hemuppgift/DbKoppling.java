package hemuppgift;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;

public class DbKoppling {

	public static String databas = "bokningssystem";
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String user = "root";
	private static String pass = "elev";
	private static Connection conn;
	private static Statement myStmt;
	String[] namn;
	
	//Uppr�ttar anslutning till databas.
	public DbKoppling() {

		try {
			conn = DriverManager.getConnection(url, user, pass);
			Class.forName("com.mysql.jdbc.Driver");
			myStmt = conn.createStatement();

		} catch (ClassNotFoundException | SQLException exc) {
			exc.printStackTrace();
		}
	}
	
	//Skapar databas samt tabeller om ej finns samt l�gger till en del skr�pdata.
	public void skapaDb() {

		try {
			myStmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + databas
					+ ";");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flygplats(flygplats_id INT NOT NULL AUTO_INCREMENT, flygplats_namn VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (flygplats_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flygbolag(flygbolag_id INT NOT NULL AUTO_INCREMENT,flygbolag_namn VARCHAR(45) UNIQUE NOT NULL,PRIMARY KEY (flygbolag_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flyglinje(flyglinje_id INT NOT NULL AUTO_INCREMENT, avgang VARCHAR(45) NOT NULL, destination VARCHAR(45) NOT NULL, flygbolag VARCHAR(45) NOT NULL, datum VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (flyglinje_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".kund(kund_id INT NOT NULL AUTO_INCREMENT,fornamn VARCHAR(45) NOT NULL,efternamn VARCHAR(45) NOT NULL,telefonnummer VARCHAR(45) NOT NULL, PRIMARY KEY (kund_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".bokning(bokning_id INT NOT NULL AUTO_INCREMENT,kund_id VARCHAR(45) NOT NULL,flyglinje VARCHAR(45) NOT NULL,PRIMARY KEY (bokning_id));");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('OSD','ARN','Norwegian','2015-01-01');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('UME','BRM','Lufthansa','2015-10-10');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('ARN','OSD','ScandinavianSAS','2015-09-21');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('OSD','UME','Lufthansa','2015-08-15');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('ARN','BRM','ScandinavianSAS','2015-07-12');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('UME','ARN','Lufthansa','2015-04-06');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('BRM','OSD','ScandinavianSAS','2015-3-20');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygbolag, datum) VALUES ('BRM','UME','Norwegian','2015-02-25');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygplats(flygplats_namn) VALUES ('UME');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygplats(flygplats_namn) VALUES ('ARN');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygplats(flygplats_namn) VALUES ('OSD');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygplats(flygplats_namn) VALUES ('BRM');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygbolag(flygbolag_namn) VALUES ('ScandinavianSAS');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygbolag(flygbolag_namn) VALUES ('Lufthansa');");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flygbolag(flygbolag_namn) VALUES ('Norwegian');");
		
		
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	//Matar in i databas och returnerar PK tillh�rande inmatad rad. 
	public int inmatningDbPk(String query, String[] pkColName) {
		int primaryKey = 0;
		try {
			java.sql.PreparedStatement prepp = conn.prepareStatement(query, pkColName);
			prepp.executeUpdate();
			ResultSet generatedKeys = prepp.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
			     primaryKey = generatedKeys.getInt(1);
			     }
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return primaryKey;
	}
	//Generell metod f�r inmatning i db.
	public void inmatningDb(String query) {
		
		try {
			myStmt.executeUpdate(query);
		
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}	
	
	public String[] listFlights(String query)
	{
		int aCounter = 0;
		try {
			
			ResultSet myRs = myStmt.executeQuery(query);
			myRs.last();
			int rows = myRs.getRow();
			ResultSetMetaData myMeta = myRs.getMetaData();
			int columns = myMeta.getColumnCount();
			namn = new String[rows];
			
			myRs.beforeFirst();
			String rowText = "";
			
			while (myRs.next()) {
				for (int i = 0; i < columns; i++) {
					rowText += (myRs.getString(i + 1) + "\t");
				}
				namn[aCounter]  = rowText;
				aCounter++;
				rowText = "";
			}
			
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return namn;
	}
	//Metod f�r att h�mta fr�n db.
	public String[] hamtaData(String query) {
		Scanner scanner = new Scanner(System.in);
		int columnsXrows = 0;
		int aCounter = 0;
		int columns = 0;
		try {
			ResultSet myRs = myStmt.executeQuery(query);
			ResultSetMetaData myMeta = myRs.getMetaData();
			columnsXrows = myMeta.getColumnCount();
			columns = columnsXrows;
			myRs.last();
			columnsXrows *= myRs.getRow();
			namn = new String[columnsXrows];
			myRs.beforeFirst();
			while (myRs.next()) {
				
				for (int i = 0; i < columns; i++) {
					namn[aCounter] = (myRs.getString(i + 1));
					aCounter++;
				}
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}

		return namn;
	}

	public void listaFlyglinjer() {

		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".flyglinje");
			System.out.println("----Flyglinjer----");
			while (myRs.next()) {

				int idflyg = myRs.getInt("flyglinje_id");
				String avgang = myRs.getString("avgang");
				String destination = myRs.getString("destination");
				String flygbolag = myRs.getString("flygbolag");
				String datum = myRs.getString("datum");
				System.out.println(" " + idflyg + ".\t" + avgang + "\t"
						+ destination + "\t" + flygbolag + "\t" + datum + "\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void listaFlygplatser() {

		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".flygplats ORDER BY flygplats_id ASC");
			System.out.println("Tillg�ngliga flygplatser.");
			System.out.println("------------------");
			while (myRs.next()) {

				int idflyg = myRs.getInt("flygplats_id");
				String aNamn = myRs.getString("flygplats_namn");
				
				System.out.println(" " + idflyg + ".\t" + aNamn + "\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}

	}

	public void listaFlygplatsAvgang() {

		try {
			ResultSet myRs = myStmt
					.executeQuery("SELECT flyglinje_id,avgang FROM " + databas
							+ ".flyglinje");

			while (myRs.next()) {

				int idflyg = myRs.getInt("flyglinje_id");
				String aNamn = myRs.getString("avgang");

				System.out.println(" " + idflyg + ".\t" + aNamn + "\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}

	}

	public void listaFlygbolag() {

		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".flygbolag");
			System.out.println("------------------");
			System.out.println("Tillg�ngliga flygbolag.");
			while (myRs.next()) {

				int idflygbolag = myRs.getInt("flygbolag_id");
				String aNamn = myRs.getString("flygbolag_namn");
			
			System.out.println(" " + idflygbolag + ".\t" + aNamn + "\t");
			}

			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}

	}

	public void listaBokningar() {

		String query;
		query = "SELECT bokning_id, fornamn, efternamn, telefonnummer, avgang,destination,flygbolag,datum "
				+ "FROM bokningssystem.kund, bokningssystem.bokning, bokningssystem.flyglinje "
					+ "where kund.kund_id = bokning.kund_id AND bokning.flyglinje = flyglinje.flyglinje_id";
		
		System.out.println("----Bokningar-----");
		String[] seFlyg = listFlights(query);
		for (int i = 0; i < seFlyg.length; i++) {
			System.out.println(seFlyg[i]);
		}
		System.out.println("------------------\n");
	}

	public static void close() {

		try {

			conn.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}

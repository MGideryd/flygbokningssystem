package hemuppgift;

public class Flyglinje {
	
	private String namn;
	private String avgang;
	private String ankomst;
	private String flygbolag;
	private String datum;
	
	
	public Flyglinje(){
		
	}
	public String getAvgang() {
		return avgang;
	}

	public void setAvgang(String avgang) {
		this.avgang = avgang;
	}

	public String getAnkomst() {
		return ankomst;
	}

	public void setAnkomst(String ankomst) {
		this.ankomst = ankomst;
	}

	public String getFlygbolag() {
		return flygbolag;
	}

	public void setFlygbolag(String flygbolag) {
		this.flygbolag = flygbolag;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public Flyglinje(String aNamn) {

		this.namn = aNamn;
	}

	public String getNamn(){

		return this.namn;
	}

	public void setNamn(String aNamn){

		this.namn = aNamn;
	}

}

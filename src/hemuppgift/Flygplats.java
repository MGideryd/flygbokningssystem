package hemuppgift;

public class Flygplats {

	String namn;
	
	
	public Flygplats(String aNamn) {
		
		this.namn = aNamn;
	}

	public String getNamn(){
		
		return this.namn;
	}
	
	public void setNamn(String aNamn){
		
		this.namn = aNamn;
	}
}

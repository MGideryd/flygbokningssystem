package hemuppgift;
import java.util.regex.Pattern;

public class Validering {
	
	public boolean valideringFlygplats(String flygplatsNamn){
		
		Pattern airport = Pattern.compile("^[a-�A-�]{3}$");		
		return airport.matcher(flygplatsNamn).matches();
	}
	
	public boolean valideringFlygbolag(String flygbolagsNamn){
		
		Pattern airport = Pattern.compile("^[a-�A-�0-9]{6,}$");
		return airport.matcher(flygbolagsNamn).matches();
	}
	
	
}
package hemuppgift;

public class Flygbolag {

	private String namn;
	
	
	public Flygbolag(String aNamn) {
		
		this.namn = aNamn;
	}

	public String getNamn(){
		
		return this.namn;
	}
	
	public void setNamn(String aNamn){
		
		this.namn = aNamn;
	}

}
